<?php

namespace Shishamou\BitbucketHook;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BitbucketController extends Controller
{
    /**
     * 觸發事件運行 git pull
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function hook(Request $request)
    {
        $xEvent = $request->header('X-Event-Key');
        $payload = json_decode($request->getContent());

        if ($xEvent != 'repo:push') {
            return response()->json(['message' => '略過處理非 repo:push 事件']);
        }

        event(new BitbucketHookEvent($payload));

        return response()->json(['message' => 'ok!']);
    }

    /**
     * 不觸發事件，將請求保存在 storage/logs/webhook.log
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function hookTest(Request $request)
    {
        $content = [
            'time' => date('Y-m-d G:i:s'),
            'headers' => array_only($request->header(), [
                'user-agent',
                'x-hook-uuid',
                'x-event-key',
                'x-request-uuid',
                'x-attempt-number'
            ]),
            'payload' => json_decode($request->getContent())
        ];
        $content = json_encode($content, JSON_PRETTY_PRINT) . "\r\n";

        file_put_contents(storage_path('logs/webhook.log'), $content, FILE_APPEND);

        return response()->json(['message' => 'ok!']);
    }
}
