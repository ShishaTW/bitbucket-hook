<?php

namespace Shishamou\BitbucketHook;

use Log;

class BitbucketHookEventListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param BitbucketWebhookEvent $event
     */
    public function handle(BitbucketHookEvent $event)
    {
        Log::info('觸發 Bitbucket Webhook 事件');

        $projectPath = base_path();
        Log::info(shell_exec("cd {$projectPath} &&  /usr/bin/git pull 2>&1"));
    }
}
