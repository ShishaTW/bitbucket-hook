<?php

namespace Shishamou\BitbucketHook;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class BitbucketHookServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Shishamou\BitbucketHook\BitbucketHookEvent' => [
            'Shishamou\BitbucketHook\BitbucketHookEventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make(BitbucketController::class);

        include(__DIR__ . '/routes.php');

        parent::boot();
    }
}
