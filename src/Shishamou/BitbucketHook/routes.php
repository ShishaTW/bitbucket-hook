<?php

Route::post('/bitbucket/webhook', 'Shishamou\BitbucketHook\BitbucketController@hook');
Route::post('/bitbucket/webhook-test', 'Shishamou\BitbucketHook\BitbucketController@hookTest');
