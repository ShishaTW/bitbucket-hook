<?php

namespace Shishamou\BitbucketHook;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class BitbucketHookEvent
{
    use Dispatchable, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($payload = [])
    {
        $this->payload = $payload;
    }
}
