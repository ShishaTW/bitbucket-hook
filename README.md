
# 簡易的處理 Bitbucket Webhook

簡易到沒有任何安全驗證
收到 repo:push 事件後會在專案主目錄進行 git pull


## 使用

請在 `config/app.php` 手動增加

```
Shishamou\BitbucketHook\BitbucketHookServiceProvider::class,
```

預設路由為 `/bitbucket/webhook` 與 `/bitbucket/webhook-test`